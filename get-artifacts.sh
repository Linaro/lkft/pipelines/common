#!/bin/bash

set -euxo pipefail

if [ ! -v CI_PROJECT_PATH ]; then
  if [ $# -gt 0 ]; then
    CI_PROJECT_PATH="$(echo "$1" | sed -e 's#^https://gitlab.com/##g' -e 's#/\-/pipelines/[0-9]*$##g')"
    CI_PIPELINE_ID="${1##*/}"
    CI_API_V4_URL="https://gitlab.com/api/v4"
  else
    echo "Usage:"
    echo "  $0 https://gitlab/project/-/pipelines/1234"
    echo "or run it from within a Gitlab CI pipeline."
    exit 1
  fi
fi

project_id="${CI_PROJECT_PATH//\//%2F}"
project_url="${CI_API_V4_URL}/projects/${project_id}"

# get list of jobs for this project
curl -f --silent \
     --output jobs.json \
     "${project_url}/pipelines/${CI_PIPELINE_ID}/jobs?per_page=100"

for job_id in $(jq '.[] | [select(.stage=="build" and .status!="manual")]' jobs.json | jq '.[] | select(.name | endswith("rootfs") | not).id'); do
  echo "Trying $job_id..."
  # get the job artifacts
  if curl -f --silent \
       --output artifacts.zip \
       --location "${project_url}/jobs/${job_id}/artifacts"; then
    break
  fi
done

# extract the files we want
unzip -o artifacts.zip build.json
