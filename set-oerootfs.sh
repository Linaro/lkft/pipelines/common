#!/bin/bash

set -x

if [ -f tuxoe-build.json ]; then
  rootfs_tuxplan_id="$(jq -r .builds[].plan tuxoe-build.json)"
  full_project="$(jq -r .builds[].project tuxoe-build.json)"
  project="${full_project/linaro\//}"

  export ROOTFS_TUXPLAN_ID="${rootfs_tuxplan_id}"
  export TUXSUITE_PROJECT="${project}"
else
  echo "Cannot find tuxoe-build.json."
  exit 1
fi
